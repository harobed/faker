import yaml
import requests
from pyquery import PyQuery as pq


def extract_firstname():
    r = requests.get('https://fr.wikipedia.org/wiki/Liste_des_pr%C3%A9noms_francophones')
    d = pq(r.text)
    for row in d('H2'):
        h2_title = pq('.mw-headline', row).text()
        if h2_title and len(h2_title) == 1:
            for dl in pq(row).next('DL'):
                for dd in pq(dl).find('DD'):
                    yield {
                        'gender': 'm' if pq(dd).find('SPAN').text() == '(m)' else 'f',
                        'firstname': unicode(pq(dd).find('B').text()).split(' ')[0]
                    }

result = [item for item in extract_firstname()]
print(
    yaml.safe_dump(
        result,
        allow_unicode=True,
        default_flow_style=False
    )
)
