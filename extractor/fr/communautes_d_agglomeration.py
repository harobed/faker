# -*- coding: utf-8 -*-
import yaml
import requests
from pyquery import PyQuery as pq


def extract_communautes_d_agglomeration():
    r = requests.get('http://fr.wikipedia.org/wiki/Liste_des_communaut%C3%A9s_d%27agglom%C3%A9ration_par_r%C3%A9gion')
    d = pq(r.text)
    for region_node in d('H2'):
        region_title = pq('.mw-headline', region_node).text()
        if region_title is None:
            continue
        if region_title == u'Légende':
            break

        for next_node in pq(region_node).nextAll():
            if next_node.tag == 'h2':
                break

            if next_node.tag == 'dl':
                departement_title = pq(next_node).text()

            if next_node.tag == 'ul':
                for li in pq(next_node).find('li'):
                    yield {
                        'region': region_title,
                        'departement': departement_title,
                        'communautes_d_agglomeration': pq(li[0]).text()
                    }


result = [item for item in extract_communautes_d_agglomeration()]
print(
    yaml.safe_dump(
        result,
        allow_unicode=True,
        default_flow_style=False
    )
)
