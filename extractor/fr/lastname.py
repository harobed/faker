import yaml
import requests
from pyquery import PyQuery as pq


def extract_lastname():
    r = requests.get('http://www.nom-famille.com/noms-les-plus-portes-en-france.html')
    d = pq(r.text)
    for a in d('.pagecentrale TD.nom'):
        yield {
            'lastname': pq(a).text(),
            'popularity': int((pq(a).next().text()).replace(u'\xa0', ''))
        }

result = [item for item in extract_lastname()]
print(
    yaml.safe_dump(
        result,
        allow_unicode=True,
        default_flow_style=False
    )
)
