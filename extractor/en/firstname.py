import yaml
import requests
from pyquery import PyQuery as pq


def extract_firstname():
    r = requests.get('http://www.ssa.gov/OACT/babynames/decades/century.html')
    d = pq(r.text)
    for row in d('TABLE[summary="Popular names for births in 1913-2012"] TBODY TR'):
        if row[0].text is None:
            break

        yield {
            'gender': 'm',
            'firstname': row[1].text,
            'popularity': int(row[2].text.replace(",", ""))
        }
        yield {
            'gender': 'f',
            'firstname': row[3].text,
            'popularity': int(row[4].text.replace(",", ""))
        }

result = [item for item in extract_firstname()]
print(
    yaml.safe_dump(
        result,
        allow_unicode=True,
        default_flow_style=False
    )
)
