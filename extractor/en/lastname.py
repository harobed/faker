import yaml
import requests
from pyquery import PyQuery as pq


def extract_lastname():
    r = requests.get('http://names.mongabay.com/data/1000.html')
    d = pq(r.text)
    for row in d('TABLE.boldtable TR'):
        if row[0].text.strip() == 'Name':
            continue

        yield {
            'lastname': row[0].text.strip().lower().title(),
            'popularity': int(row[2].text.strip())
        }

result = [item for item in extract_lastname()]
print(
    yaml.safe_dump(
        result,
        allow_unicode=True,
        default_flow_style=False
    )
)
