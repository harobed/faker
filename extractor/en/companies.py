import csv
import yaml
import requests


def extract_compagnies():
    r = requests.get('http://cs.brown.edu/~pavlo/fortune1000/fortune1000_companies.csv')
    reader = csv.reader(r.text.split('\n')[1:], delimiter='\t')
    for row in reader:
        if len(row) == 10:
            c = False
            for v in row:
                if (v == 'None') or (v == ''):
                    c = True
                    break
            if c:
                continue

            if not row[6].startswith('http://www.'):
                continue

            yield {
                'name': row[0],
                'address': row[1],
                'city': row[2],
                'state': row[3],
                'zipcode': int(row[4]),
                'phone': row[5],
                'domain': row[6][11:].lower(),
                'general email': row[7].lower(),
                'ceo name': row[8],
                'ceo email': row[9]
            }

result = [item for item in extract_compagnies()]
print(
    yaml.safe_dump(
        result,
        allow_unicode=True,
        default_flow_style=False
    )
)
