=====
Faker
=====


Firstname and lastname
======================

::

    >>> from faker import firstname
    >>> firstname()
    u'Judith'

    >>> firstname(gender='m')
    u'Sean'

    >>> firstname(gender='f')
    u'Shirley'

    >>> from faker import lastname
    >>> lastname()
    u'Potts'

    >>> from faker import name
    >>> name()
    (u'Joe', u'Richmond')

    >>> name(gender='f')
    (u'Beverly', u'Matthews')


Select another lang :

::

    >>> firstname(lang='fr')
    u'Magali'

Change global settings :

::

    >>> from faker import settings
    >>> settings['lang'] = 'fr'
    >>> lastname()
    u'Le gall'

    >>> lastname()
    u'Simon'


Companies
=========

::

    >>> settings['lang'] = 'en'
    >>> from faker import companies
    >>> companies.name()
    u'Bank of America Corp.'
    
    >>> companies.city()
    u'Framingham'

    >>> companies.domain()
    u'dow.com'

    >>> companies()
    {u'city': u'Plano', u'domain': u'eds.com', u'name': u'Electronic Data Systems', u'state': u'TX', u'zipcode': 75024, u'ceo name': u'Ronald A. Rittenmeyer', u'phone': u'972-604-6000', u'ceo email': u'ronald.rittenmeyer@eds.com', u'address': u'5400 Legacy Dr.', u'general email': u'info@eds.com'}


Mail
====

::

    >>> from faker import mail
    >>> mail()
    u'joshua.page@cbre.com'

    >>> mail(firstname='linus', lastname='torvalds')
    u'linus.torvalds@albemarle.com'
