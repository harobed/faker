from fabric.api import task, local


@task
def extract_all():
    local('bin/python extractor/fr/firstname.py > faker/datas/fr_firstname.yaml')
    local('bin/python extractor/fr/lastname.py > faker/datas/fr_lastname.yaml')
    local('bin/python extractor/fr/communautes_d_agglomeration.py > faker/datas/fr_communautes_d_agglomeration.yaml')

    local('bin/python extractor/en/firstname.py > faker/datas/en_firstname.yaml')
    local('bin/python extractor/en/lastname.py > faker/datas/en_lastname.yaml')
    local('bin/python extractor/en/companies.py > faker/datas/en_companies.yaml')
