__version__ = '0.1.1'
import os
import unicodedata

import yaml
import faker.utils

here = os.path.dirname(__file__)

datas = {}
settings = {
    'lang': 'en',
    'random': faker.utils.FakeRandom()
}


def construct_yaml_str(self, node):
    return self.construct_scalar(node)

yaml.Loader.add_constructor(u'tag:yaml.org,2002:str', construct_yaml_str)
yaml.SafeLoader.add_constructor(u'tag:yaml.org,2002:str', construct_yaml_str)


def _get_lang(lang=None):
    if lang:
        return lang
    return settings['lang']


def _get_random(random=None):
    if random:
        return random
    return settings['random']


def firstname(gender=None, lang=None, random=None):
    lang = _get_lang(lang)
    random = _get_random(random)

    list_name = '%s_firstname' % lang

    if list_name not in datas:
        if lang in ('en', 'fr'):
            f = open(os.path.join(here, 'datas', '%s_firstname.yaml' % lang), 'r')
            datas[list_name] = yaml.load(f)
            f.close()
            datas[list_name + '_m'] = [v for v in datas[list_name] if v['gender'] == 'm']
            datas[list_name + '_f'] = [v for v in datas[list_name] if v['gender'] == 'f']

        else:
            raise Exception('Lang "%s" not supported' % lang)

    if gender == 'm':
        return random.choice(datas[list_name + '_m'])['firstname']
    elif gender == 'f':
        return random.choice(datas[list_name + '_f'])['firstname']
    else:
        return random.choice(datas[list_name])['firstname']

_firstname = firstname


def lastname(lang=None, random=None):
    lang = _get_lang(lang)
    random = _get_random(random)

    list_name = '%s_lastname' % lang

    if list_name not in datas:
        if lang in ('en', 'fr'):
            f = open(os.path.join(here, 'datas', '%s_lastname.yaml' % lang), 'r')
            datas[list_name] = yaml.load(f)
            f.close()

        else:
            raise Exception('Lang "%s" not supported' % lang)

    return random.choice(datas[list_name])['lastname']

_lastname = lastname


def name(gender=None, lang=None, random=None):
    lang = _get_lang(lang)
    random = _get_random(random)

    return (
        firstname(gender, lang, random),
        lastname(lang, random)
    )


def mail(gender=None, firstname=None, lastname=None, domain=None, lang=None, random=None):
    lang = _get_lang(lang)
    random = _get_random(random)

    if firstname is None:
        firstname = _firstname(gender, lang, random)

    if lastname is None:
        lastname = _lastname(lang, random)

    if domain is None:
        domain = companies.domain(lang, random)

    return ('%s.%s@%s' % (remove_accents(firstname), remove_accents(lastname), domain)).lower()


def _load_companies(lang):
    list_name = '%s_companies' % lang
    if list_name not in datas:
        if lang in ('en',):
            f = open(os.path.join(here, 'datas', '%s_companies.yaml' % lang), 'r')
            datas[list_name] = yaml.load(f)
            f.close()

        else:
            raise Exception('Lang "%s" not supported' % lang)


class companies(dict):
    def __init__(self, lang=None, random=None):
        lang = _get_lang(lang)
        random = _get_random(random)

        _load_companies(lang)

        result = random.choice(
            datas['%s_companies' % lang]
        )

        self.update(result)

    @staticmethod
    def name(lang=None, random=None):
        c = companies(lang, random)
        return c['name']

    @staticmethod
    def city(lang=None, random=None):
        return companies(lang, random)['city']

    @staticmethod
    def domain(lang=None, random=None):
        return companies(lang, random)['domain']

    @staticmethod
    def phone(lang=None, random=None):
        return companies(lang, random)['phone']

    @staticmethod
    def address(lang=None, random=None):
        return companies(lang, random)['address']


def remove_accents(input_str):
    nkfd_form = unicodedata.normalize('NFKD', unicode(input_str))
    return u"".join([c for c in nkfd_form if not unicodedata.combining(c)])
