import os
from datetime import datetime, timedelta

here = os.path.dirname(__file__)
random_values = map(int, open(os.path.join(here, 'datas', 'random_values.txt')).read().split(', '))


class FakeRandom(object):
    def __init__(self, fake_today=None):
        self.cursor = -1
        if fake_today:
            self.fake_today = fake_today
        else:
            self.fake_today = datetime.today()

    def reset(self):
        self.cursor = -1

    def randint(self, a, b):
        self.cursor += 1
        v = random_values[self.cursor % len(random_values)]
        return int(round((v * float(b - a) / 10000)) + a)

    def random_date(self, delta, offset=0):
        return (
            self.fake_today +
            timedelta(
                offset
            ) +
            timedelta(
                (1 if delta > 0 else -1) * self.randint(0, abs(delta))
            )
        )

    def choice(self, choices):
        return choices[self.randint(0, len(choices) - 1)]


class UUID(object):
    def __init__(self):
        self.counter = 0

    def reset(self):
        self.counter = 0

    def uuid4(self):
        self.counter += 1
        return '00000000-0000-0000-0000-%012d' % self.counter
